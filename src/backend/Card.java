/**
 * @author Martin Paakspuu
 * @version 1.0
 */

package backend;

public class Card {
	/** kaardi mast */
	public String suit;
	/** kaardi v��rtus */
	public String value;
	/** kaardi number */
	public int number;
	/** pildil oleva kaardi koordinaatide massiiv */
	public int[] coordinates;

	/**
	 * Card t��pi objekti konstruktor
	 * 
	 * @param nr
	 *            suvaline number (1-52)
	 */
	public Card(int nr) {
		number = nr;
		getImageCoordinates();
	}

	/**
	 * Tagastab antud kaardi masti numbrina (poti on 0, �rtu on 1, risti on 2,
	 * ruutu on 3)
	 * 
	 * @return kaardi masti number
	 */
	public int getSuitnr() {
		int suit = 0;
		if (number <= 13 && number > 0) {
			suit = 0;
		}
		if (number > 13 && number <= 26) {
			suit = 1;
		}
		if (number > 26 && number <= 39) {
			suit = 2;
		}
		if (number > 39 && number <= 52) {
			suit = 3;
		}
		return suit;
	}

	/**
	 * Tagastab kaardi masti stringina
	 * 
	 * @return kaardi mast
	 */
	public String getSuit() {
		int x = getSuitnr();
		String suit = null;
		switch (x) {
		case 0:
			suit = "Clubs";
			break;
		case 1:
			suit = "Hearts";
			break;
		case 2:
			suit = "Spades";
			break;
		case 3:
			suit = "Diamonds";
			break;
		}
		return suit;
	}

	/**
	 * Tagastab kaardi v��rtuse stringina
	 * 
	 * @return kaardi v��rtus
	 */
	public String getValue() {
		int nr = number % 13;
		String value = null;
		switch (nr) {
		case 0:
			value = "K";
			break;
		case 1:
			value = "A";
			break;
		case 2:
			value = "2";
			break;
		case 3:
			value = "3";
			break;
		case 4:
			value = "4";
			break;
		case 5:
			value = "5";
			break;
		case 6:
			value = "6";
			break;
		case 7:
			value = "7";
			break;
		case 8:
			value = "8";
			break;
		case 9:
			value = "9";
			break;
		case 10:
			value = "10";
			break;
		case 11:
			value = "J";
			break;
		case 12:
			value = "Q";
			break;
		}

		return value;
	}

	/**
	 * Tagastab antud kaardi v��rtuse ja masti stringina
	 * 
	 * @return kaardi v��rtus ja mast
	 */
	public String getCard() {
		String card = null;
		card = getValue() + " " + getSuit();
		return card;
	}

	/**
	 * Tagastab antud kaardi koordinaadid pildil
	 * 
	 * @return kaardi koordinaatide massiiv
	 */
	private int[] getImageCoordinates() {
		coordinates = new int[2];
		if (number % 13 == 0) {
			coordinates[0] = 12 * 73 + 1;
		} else {
			coordinates[0] = (number % 13 - 1) * 73 + 1;
		}
		coordinates[1] = getSuitnr() * 98 + 1;
		return coordinates;
	}
}
