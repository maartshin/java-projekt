/**
 * @author Martin Paakspuu
 * @version 1.0
 */

package backend;

public class Player {
	/** kaartide massiivi kloon */
	private Card[] copy;
	/** sorteeritud kaardiv��rtuste massiiv */
	private int[] cardvalues;
	/** raha kogus */
	public int balance = 1000;
	/** esimest sorti korduvate kaartide arv */
	private int countone;
	/** teist sorti korduvate kaartide arv */
	private int counttwo;
	/** esimest sorti korduvate kaartide v��rtus */
	private int paircard1;
	/** teist sorti korduvate kaartide v��rtus */
	private int paircard2;
	/** esimest sorti korduvate kaartide v��rtus stringina */
	private String pair1;
	/** teist sorti korduvate kaartide v��rtus stringina */
	private String pair2;
	/** k�e tugevus stringina */
	public String hand;

	/**
	 * Player t��pi objekti konstruktor
	 */
	public Player() {

	}

	/**
	 * Sisestab uued kaardid objekti
	 * 
	 * @param cards
	 *            kaartide massiiv
	 */
	public void getHand(Card[] cards) {
		copy = cards.clone();
		rida();
		isPairs();
	}

	/**
	 * Kontrollib, kas k�es on kuninglik rida
	 * 
	 * @return kuningliku rea t�ev��rtus
	 */
	private boolean isRoyalStraight() {
		boolean royalstraight = false;
		if (cardvalues[0] == 1 && cardvalues[1] == 10 && cardvalues[2] == 11 && cardvalues[3] == 12
				&& cardvalues[4] == 13) {
			royalstraight = true;
		}
		return royalstraight;
	}

	/**
	 * Kontrollib, kas k�es on rida
	 * 
	 * @return rea t�ev��rtus
	 */
	private boolean isStraight() {
		boolean straight = true;
		for (int i = 0; i < 4; i++) {
			if (cardvalues[i] + 1 != cardvalues[i + 1]) {
				straight = false;
			}
		}
		if (isRoyalStraight()) {
			straight = true;
		}
		return straight;
	}

	/**
	 * Sorteerib integer t��pi massiivi kasvavasse j�rjekorda
	 * 
	 * @param card
	 *            integer massiiv
	 */
	private void sortHand(int[] card) {
		int x = 0;
		int y = 0;
		for (int i = 0; i < 5; i++) {
			x = i;
			for (int j = i; j < 5; j++) {
				if (card[x] > card[j]) {
					x = j;
				}
			}
			y = card[i];
			card[i] = card[x];
			card[x] = y;
		}
	}

	/**
	 * Kontrollib, kas k�es on mast
	 * 
	 * @return masti t�ev��rtus
	 */
	private boolean isSuit() {
		boolean suit = true;
		for (int i = 0; i < 4; i++) {
			if (copy[i].getSuitnr() != copy[i + 1].getSuitnr()) {
				suit = false;
			}
		}
		return suit;
	}

	/**
	 * Salvestab antud k�e kaartide v��rtused massiivi ning sorteerib selle
	 */
	private void rida() {
		cardvalues = new int[5];
		for (int i = 0; i < 5; i++) {
			cardvalues[i] = copy[i].number % 13;
			if (cardvalues[i] == 0) {
				cardvalues[i] = 13;
			}
		}
		sortHand(cardvalues);
	}

	/**
	 * Kontrollib, kas k�es samasuguste v��rtustega kaarte
	 */
	private void isPairs() {
		int duplicatea = 0;
		int duplicateb = 0;
		int x = 1;
		int y = 1;
		int counta = 1;
		int countb = 1;
		for (int i = 0; i < 4; i++) {
			x = 1;
			for (int j = i + 1; j < 5; j++) {
				if (cardvalues[i] != cardvalues[j]) {
					break;
				} else {
					x += 1;
				}
				if (counta < x) {
					counta = x;
					duplicatea = cardvalues[i];
				}
			}
		}
		for (int i = 0; i < 4; i++) {
			y = 1;
			if (cardvalues[i] != duplicatea) {
				for (int j = i + 1; j < 5; j++) {
					if (cardvalues[i] != cardvalues[j]) {
						break;
					} else {
						y += 1;
					}
					if (countb < y) {
						countb = y;
						duplicateb = cardvalues[i];
					}
				}
			}
		}
		countone = counta;
		counttwo = countb;
		paircard1 = duplicatea;
		paircard2 = duplicateb;
		pair1 = cardToString(duplicatea);
		pair2 = cardToString(duplicateb);
	}

	/**
	 * Kontrollib, kas k�es on kuninglik mastirida
	 * 
	 * @return kuningliku mastirea t�ev��rtus
	 */
	private boolean isRoyalFlush() {
		return (isRoyalStraight() && isSuit());
	}

	/**
	 * Kontrollib, kas k�es on mastirida
	 * 
	 * @return mastirea t�ev��rtus
	 */
	private boolean isStraightFlush() {
		return (isStraight() && isSuit());
	}

	/**
	 * Kontrollib, kas k�es on nelik
	 * 
	 * @return neliku t�ev��rtus
	 */
	private boolean isQuadra() {
		return (countone == 4);
	}

	/**
	 * Kontrollib, kas k�es on maja
	 * 
	 * @return maja t�ev��rtus
	 */
	private boolean isFullHouse() {
		return (countone == 3 && counttwo == 2);
	}

	/**
	 * Kontrollib, kas k�es on kolmik
	 * 
	 * @return kolmiku t�ev��rtus
	 */
	private boolean isTriple() {
		return (countone == 3 && counttwo != 2);
	}

	/**
	 * Kontrollib, kas k�es on kaks paari
	 * 
	 * @return kahe paari t�ev��rtus
	 */
	private boolean isTwoPair() {
		return (countone == 2 && counttwo == 2);
	}

	/**
	 * Kontrollib, kas k�es on �ks paar
	 * 
	 * @return paari t�ev��rtus
	 */
	private boolean isOnePair() {
		return (countone == 2 && counttwo == 1);
	}

	/**
	 * V�ljastab parima k�e ning uuendab raha kogust
	 * 
	 * @param bet
	 *            panus
	 */
	public void Result(int bet) {
		int winning;
		if (isRoyalFlush()) {
			hand = "Royal flush";
			winning = 60 * bet;
		} else if (isStraightFlush()) {
			hand = "Straight flush " + cardToString(cardvalues[0]) + " to " + cardToString(cardvalues[4]);
			winning = 30 * bet;
		} else if (isQuadra()) {
			hand = "Four of a kind " + pair1 + "'s";
			winning = 20 * bet;
		} else if (isFullHouse()) {
			hand = "Full house " + pair1 + "'s full of " + pair2 + "'s";
			winning = 15 * bet;
		} else if (isSuit()) {
			hand = "Flush " + copy[1].getSuit();
			winning = 10 * bet;
		} else if (isStraight()) {
			if (isRoyalStraight()) {
				hand = "Straight " + cardToString(cardvalues[1]) + " to " + cardToString(cardvalues[0]);
			} else {
				hand = "Straight " + cardToString(cardvalues[0]) + " to " + cardToString(cardvalues[4]);
			}
			winning = 7 * bet;
		} else if (isTriple()) {
			hand = "Three of a kind " + pair1 + "'s";
			winning = 3 * bet;
		} else if (isTwoPair()) {
			hand = "Two Pair " + pair1 + "'s and " + pair2 + "'s";
			winning = 2 * bet;
		} else if (isOnePair()) {
			hand = "One pair " + pair1 + "'s";
			if (paircard1 >= 11 || paircard1 == 1) {
				winning = bet;
			} else {
				winning = -bet;
			}
		} else {
			if (cardvalues[0] == 1) {
				hand = "High card " + cardToString(cardvalues[0]);
			} else {
				hand = "High card " + cardToString(cardvalues[4]);
			}
			winning = -bet;
		}
		balance += winning;
	}

	/**
	 * Teisendab kaardi numbri stringiks
	 * 
	 * @param card
	 *            kaardid
	 * 
	 * @return kaardi v��rtus stringina
	 */
	private String cardToString(int card) {
		String newcard = "";
		if (card == 1) {
			newcard = "A";
		} else if (card == 11) {
			newcard = "J";
		} else if (card == 12) {
			newcard = "Q";
		} else if (card == 13) {
			newcard = "K";
		} else {
			newcard = Integer.toString(card);
		}
		return newcard;
	}
}
