/**
 * @author Martin Paakspuu
 * @version 1.0
 */
package backend;

public class Hand {
	int cards = 5;
	/** k�es olevad kaardid */
	public Card[] hand;
	/** massiiv, mis kirjeldab, milline kaart vahetatakse v�lja */
	public boolean[] cardStatus;
	/** k�es olevad kaardid */
	public String[] kaardid;

	/**
	 * Hand t��pi objekti konstruktor
	 * 
	 * @param cards
	 *            kaardi objektide massiiv
	 */
	public Hand(Card[] cards) {
		hand = cards;
		getCardStatus();
	}

	/**
	 * Muudab k�ik kaardid v�ljavahetatavaks
	 */
	public void getCardStatus() {
		cardStatus = new boolean[cards];
		for (int i = 0; i < cards; i++) {
			cardStatus[i] = true;
		}
	}

	/**
	 * Muudab kaardi v�ljavahetatavaks v�i v�ljavahetamatuks
	 * 
	 * @param nr
	 *            kaardi asukoht
	 */
	public void changeCardStatus(int nr) {
		if (cardStatus[nr] == true) {
			cardStatus[nr] = false;
		} else if (cardStatus[nr] == false) {
			cardStatus[nr] = true;
		}
	}

	/**
	 * Tagastab k�e massiivi stringina
	 * 
	 * @return k�es olevad kaardid
	 */
	public String[] getHandCards() {
		kaardid = new String[cards];
		for (int i = 0; i < cards; i++) {
			kaardid[i] = hand[i].getCard();
		}
		return kaardid;
	}
}
