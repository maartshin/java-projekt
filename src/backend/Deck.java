/**
 * @author Martin Paakspuu
 * @version 1.0
 */
package backend;

public class Deck {
	/** pakis olevate kaartide arv */
	private int cards = 52;
	/** j�relej��nud kaartide arv */
	public int cardsleft = cards;
	/** kaardipakk, milles on 52 kaardi objekti */
	public Card[] deck;
	/** k�si, milles on 5 kaardi objekti */
	public Card[] hand;
	/** diilitud kaart */
	private Card dealcard;

	/**
	 * Deck t��pi objekti konstruktor
	 */
	public Deck() {
		getDeck();
		shuffle();
	}

	/**
	 * Genereerib suvalise arvu (0-51)
	 * 
	 * @return suvaline arv
	 */
	public int randomCard() {
		return (int) (Math.random() * 52);
	}

	/**
	 * Loob 52-kaardilise kaardipaki, milles on k�ik kaardid j�rjest
	 * 
	 * @return kaardipakk
	 */
	public Card[] getDeck() {
		deck = new Card[52];
		for (int i = 1; i <= 52; i++) {
			deck[i - 1] = new Card(i);
		}
		return deck;
	}

	public int getCardnr(int nr) {
		return deck[nr].number;
	}

	/**
	 * Segab kaardipaki l�bi
	 */
	public void shuffle() {
		for (int i = 0; i < 52; i++) {
			int x = randomCard();
			int randomcard = deck[x].number;
			int currentcard = deck[i].number;
			deck[i] = new Card(randomcard);
			deck[x] = new Card(currentcard);
		}
	}

	/**
	 * V�tab pakist kaardi
	 * 
	 * @return pakist v�etud kaart
	 */
	public Card Deal() {
		dealcard = deck[cardsleft - 1];
		cardsleft--;
		return dealcard;
	}

	/**
	 * Paneb kaardid pakki tagasi ja segab need
	 */
	public void Clear() {
		cardsleft = 52;
		shuffle();
	}

	/**
	 * Salvestab x arv kaarte hand massiivi
	 * 
	 * @param x
	 *            kaartide arv
	 * @return x kaardiga k�si
	 */
	public Card[] getHand(int x) {
		hand = new Card[x];
		for (int i = 0; i < x; i++) {
			hand[i] = Deal();
		}
		return hand;
	}
}
