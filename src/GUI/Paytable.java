package GUI;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;

public class Paytable extends JFrame{
	int x,y;
	
	public void getXY(int x0,int y0){
		x=x0;
		y=y0;
		setLocation(x,y);
	}
	public Paytable(){
		super("Paytable");
		setSize(300,500);
		setResizable(false);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 24, 0, 34, 0, 0};
		gridBagLayout.rowHeights = new int[]{65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblPaytable = new JLabel("Paytable");
		lblPaytable.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblPaytable = new GridBagConstraints();
		gbc_lblPaytable.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaytable.gridx = 2;
		gbc_lblPaytable.gridy = 0;
		getContentPane().add(lblPaytable, gbc_lblPaytable);
		
		JLabel lblRoyalFlush = new JLabel("Royal Flush");
		lblRoyalFlush.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblRoyalFlush = new GridBagConstraints();
		gbc_lblRoyalFlush.anchor = GridBagConstraints.WEST;
		gbc_lblRoyalFlush.insets = new Insets(0, 0, 5, 5);
		gbc_lblRoyalFlush.gridx = 2;
		gbc_lblRoyalFlush.gridy = 2;
		getContentPane().add(lblRoyalFlush, gbc_lblRoyalFlush);
		
		JLabel lblx = new JLabel("60x");
		lblx.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx = new GridBagConstraints();
		gbc_lblx.insets = new Insets(0, 0, 5, 0);
		gbc_lblx.gridx = 4;
		gbc_lblx.gridy = 2;
		getContentPane().add(lblx, gbc_lblx);
		
		JLabel lblStraightFlush = new JLabel("Straight flush");
		lblStraightFlush.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblStraightFlush = new GridBagConstraints();
		gbc_lblStraightFlush.insets = new Insets(0, 0, 5, 5);
		gbc_lblStraightFlush.anchor = GridBagConstraints.WEST;
		gbc_lblStraightFlush.gridx = 2;
		gbc_lblStraightFlush.gridy = 3;
		getContentPane().add(lblStraightFlush, gbc_lblStraightFlush);
		
		JLabel lblx_1 = new JLabel("30x");
		lblx_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_1 = new GridBagConstraints();
		gbc_lblx_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_1.gridx = 4;
		gbc_lblx_1.gridy = 3;
		getContentPane().add(lblx_1, gbc_lblx_1);
		
		JLabel lblFourOfA = new JLabel("Four of a kind");
		lblFourOfA.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblFourOfA = new GridBagConstraints();
		gbc_lblFourOfA.anchor = GridBagConstraints.WEST;
		gbc_lblFourOfA.insets = new Insets(0, 0, 5, 5);
		gbc_lblFourOfA.gridx = 2;
		gbc_lblFourOfA.gridy = 4;
		getContentPane().add(lblFourOfA, gbc_lblFourOfA);
		
		JLabel lblx_2 = new JLabel("20x");
		lblx_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_2 = new GridBagConstraints();
		gbc_lblx_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_2.gridx = 4;
		gbc_lblx_2.gridy = 4;
		getContentPane().add(lblx_2, gbc_lblx_2);
		
		JLabel lblFullHouse = new JLabel("Full house");
		lblFullHouse.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblFullHouse = new GridBagConstraints();
		gbc_lblFullHouse.anchor = GridBagConstraints.WEST;
		gbc_lblFullHouse.insets = new Insets(0, 0, 5, 5);
		gbc_lblFullHouse.gridx = 2;
		gbc_lblFullHouse.gridy = 5;
		getContentPane().add(lblFullHouse, gbc_lblFullHouse);
		
		JLabel lblx_3 = new JLabel("15x");
		lblx_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_3 = new GridBagConstraints();
		gbc_lblx_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_3.gridx = 4;
		gbc_lblx_3.gridy = 5;
		getContentPane().add(lblx_3, gbc_lblx_3);
		
		JLabel lblFlush = new JLabel("Flush");
		lblFlush.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblFlush = new GridBagConstraints();
		gbc_lblFlush.anchor = GridBagConstraints.WEST;
		gbc_lblFlush.insets = new Insets(0, 0, 5, 5);
		gbc_lblFlush.gridx = 2;
		gbc_lblFlush.gridy = 6;
		getContentPane().add(lblFlush, gbc_lblFlush);
		
		JLabel lblx_4 = new JLabel("10x");
		lblx_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_4 = new GridBagConstraints();
		gbc_lblx_4.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_4.gridx = 4;
		gbc_lblx_4.gridy = 6;
		getContentPane().add(lblx_4, gbc_lblx_4);
		
		JLabel lblStraight = new JLabel("Straight");
		lblStraight.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblStraight = new GridBagConstraints();
		gbc_lblStraight.anchor = GridBagConstraints.WEST;
		gbc_lblStraight.insets = new Insets(0, 0, 5, 5);
		gbc_lblStraight.gridx = 2;
		gbc_lblStraight.gridy = 7;
		getContentPane().add(lblStraight, gbc_lblStraight);
		
		JLabel lblx_8 = new JLabel("7x");
		lblx_8.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_8 = new GridBagConstraints();
		gbc_lblx_8.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_8.gridx = 4;
		gbc_lblx_8.gridy = 7;
		getContentPane().add(lblx_8, gbc_lblx_8);
		
		JLabel lblThreeOfA = new JLabel("Three of a kind");
		lblThreeOfA.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblThreeOfA = new GridBagConstraints();
		gbc_lblThreeOfA.anchor = GridBagConstraints.WEST;
		gbc_lblThreeOfA.insets = new Insets(0, 0, 5, 5);
		gbc_lblThreeOfA.gridx = 2;
		gbc_lblThreeOfA.gridy = 8;
		getContentPane().add(lblThreeOfA, gbc_lblThreeOfA);
		
		JLabel lblx_7 = new JLabel("3x");
		lblx_7.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_7 = new GridBagConstraints();
		gbc_lblx_7.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_7.gridx = 4;
		gbc_lblx_7.gridy = 8;
		getContentPane().add(lblx_7, gbc_lblx_7);
		
		JLabel lblTwoPair = new JLabel("Two pair");
		lblTwoPair.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblTwoPair = new GridBagConstraints();
		gbc_lblTwoPair.anchor = GridBagConstraints.WEST;
		gbc_lblTwoPair.insets = new Insets(0, 0, 5, 5);
		gbc_lblTwoPair.gridx = 2;
		gbc_lblTwoPair.gridy = 9;
		getContentPane().add(lblTwoPair, gbc_lblTwoPair);
		
		JLabel lblx_6 = new JLabel("2x");
		lblx_6.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_6 = new GridBagConstraints();
		gbc_lblx_6.insets = new Insets(0, 0, 5, 0);
		gbc_lblx_6.gridx = 4;
		gbc_lblx_6.gridy = 9;
		getContentPane().add(lblx_6, gbc_lblx_6);
		
		JLabel lblOnePair = new JLabel("One pair(Jacks or better)");
		lblOnePair.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblOnePair = new GridBagConstraints();
		gbc_lblOnePair.anchor = GridBagConstraints.WEST;
		gbc_lblOnePair.insets = new Insets(0, 0, 0, 5);
		gbc_lblOnePair.gridx = 2;
		gbc_lblOnePair.gridy = 10;
		getContentPane().add(lblOnePair, gbc_lblOnePair);
		
		JLabel lblx_5 = new JLabel("1x");
		lblx_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblx_5 = new GridBagConstraints();
		gbc_lblx_5.gridx = 4;
		gbc_lblx_5.gridy = 10;
		getContentPane().add(lblx_5, gbc_lblx_5);
	}
}
