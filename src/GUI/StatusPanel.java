/**
	 * Joonistab GamePanel objekti paremasse alla nurka kasti, kust on n�ha
	 * kontoj��ki ning k�e tugevust.
	 * 
	 * @author Martin Paakspuu
	 *
	 */
package GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	/** Raha kogus */
	int balance = 1000;
	/** Parim k�si stringina */
	String hand = "";
	/** Fonti suurus */
	int font = 14;
	/** Ridade kaugus statuspanel kastis */
	int posx = 20;
	/** Esimese rea k�rgus statuspanel kastis */
	int posy = 25;
	/** Teise rea k�rgus statuspanel kastis */
	int posyy = 50;

	/**
	 * Paneb paika teksti asukoha ja suuruse
	 * 
	 * @param x
	 *            ridade kaugus
	 * @param y
	 *            esimese rea k�rgus
	 * @param yy
	 *            teise rea k�rgus
	 * @param fnt
	 *            fonti suurus
	 */
	public void setText(int x, int y, int yy, int fnt) {
		posx = x;
		posy = y;
		posyy = yy;
		font = fnt;
	}

	/**
	 * StatusPanel t��pi objekti konstruktor
	 */
	public StatusPanel() {
		this.setSize(200, 100);
		this.setLocation(300, 300);
		this.setVisible(true);
		this.setBackground(new Color(151, 202, 147, 50));
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.BLACK));
		// getRootPane().setLayout(null);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		DrawStatus(g);
	}

	/**
	 * Uuendab raha kogust
	 * 
	 * @param newbalance
	 *            uus raha kogus
	 */
	public void GetNewBalance(int newbalance) {
		balance = newbalance;
	}

	/**
	 * N�itab k�e tugevust ja raha kogust
	 */
	public void DrawStatus(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		// g2.setColor(new Color(0,0,0));
		g2.setFont(new Font("Agency FB", Font.BOLD, font));

		g2.drawString("Balance: " + String.valueOf(balance), posx, posy);
		g2.drawString("Hand: " + hand, posx, posyy);
	}

	/**
	 * Salvestab isendimuutujasse parima k�e
	 * 
	 * @param cards
	 *            parim k�si stringina
	 */
	public void getHand(String cards) {
		hand = cards;
	}

	/**
	 * L�petab k�e tugevuse n�itamise
	 */
	public void clearHand() {
		hand = "";
	}

}
