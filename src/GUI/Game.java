/**
 * @author Martin Paakspuu
 */
package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Game extends JFrame {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		new Game().setVisible(true);
	}

	/**
	 * V�ttes sisendiks tausts�steemi pikkuse v�i laiuse ning mingi objekti
	 * pikkuse v�i laiuse, tagastab meetod nende suhte.
	 * 
	 * @param base
	 *            Tausts�steemi pikkus v�i laius
	 * @param def
	 *            Objekti algpikkus v�i alglaius
	 * @return tausts�steemi ning mingi objekti suuruste suhe
	 */
	public static double reSize(int base, int def) {
		return ((double) def / (double) base);
	}

	private Game() {
		super("Jacks or Better");
		getContentPane().setBackground(new Color(51, 143, 51));
		setSize(800, 600);
		setLocation(100, 100);
		setResizable(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		JMenuBar bar = new JMenuBar();
		JMenu menu = new JMenu("Game");
		setMinimumSize(new Dimension(800, 600));

		setJMenuBar(bar);
		bar.add(menu);

		Paytable table = new Paytable();
		GamePanel game = new GamePanel();
		getContentPane().add(game);

		/**
		 * New game nuppu vajutades viiakse programm algolekusse.
		 */
		JMenuItem newgame = new JMenuItem("New game");
		newgame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.deck.Clear();
				game.allowchange = false;
				game.status = 1;
				game.hand.getCardStatus();
				game.shoulddraw = false;
				game.player.balance = 1000;
				game.statuspanel.GetNewBalance(game.player.balance);
				game.statuspanel.clearHand();
				game.betfield.setText("");
				game.betfield.setEnabled(true);
				game.deal.setEnabled(false);
				repaint();
			}
		});
		/**
		 * Paytable nuppu vajutades n�idatakse kasutajale v�itude tabelit.
		 */
		JMenuItem paytable = new JMenuItem("Paytable");
		paytable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				table.getXY(getX() + getWidth() / 2, getY() + getHeight() / 4);
				table.setVisible(true);
			}
		});
		/**
		 * Exit nuppu vajutades suletakse programm.
		 */
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		menu.add(newgame);
		menu.add(paytable);
		menu.add(exit);

		super.addComponentListener(new ComponentListener() {
			/**
			 * Muudab sisu suurust, kui akna suurust muudetakse
			 */
			public void componentResized(ComponentEvent e) {
				int width = getWidth();
				int height = getHeight();

				game.setSize((int) (reSize(800, 500) * width), (int) (reSize(600, 400) * height));
				game.setLocation((int) (reSize(800, 150) * width), (int) (reSize(600, 80) * height));
				game.cardx = (int) (reSize(800, 71) * width);
				game.cardy = (int) (reSize(71, 96) * game.cardx);
				game.marginx = (int) (reSize(800, 107) * width);
				game.deal.setBounds(game.cardx + (int) (reSize(800, 40) * width), game.getHeight() - (game.cardy / 2),
						(int) (reSize(800, 70) * width), (int) (reSize(600, 25) * height));
				game.deal.setFont(game.deal.getFont().deriveFont((float) (reSize(800, 12) * width)));
				game.animationy = game.getHeight() - game.cardy;
				game.statuspanel.setBounds((int) (reSize(800, 300) * width),
						game.getHeight() - (int) (reSize(200, 100) * (int) (reSize(800, 200) * width)),
						(int) (reSize(800, 200) * width), (int) (reSize(200, 100) * (int) (reSize(800, 200) * width)));
				game.statuspanel.setText(game.statuspanel.getWidth() / 10, game.statuspanel.getHeight() / 3,
						(game.statuspanel.getHeight() / 3) * 2, (int) (reSize(800, 20) * width));
				game.betfield.setBounds(game.cardx + (int) (reSize(800, 40) * width),
						game.getHeight() - (int) (reSize(96, 80) * game.cardy), (int) (reSize(800, 60) * width),
						(int) (reSize(600, 25) * height));
				game.betfield.setFont(game.deal.getFont().deriveFont((float) (reSize(800, 12) * width)));
			}

			@Override
			public void componentHidden(ComponentEvent e) {

			}

			@Override
			public void componentMoved(ComponentEvent e) {

			}

			@Override
			public void componentShown(ComponentEvent e) {

			}
		});
		/**
		 * Kontrollib sisestust ning lukustab deal nupu, kui sisestus ei sobi
		 */
		game.betfield.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				boolean digit = true;
				for (int i = 0; i < game.betfield.getText().length(); i++) {
					if (!Character.isDigit(game.betfield.getText().charAt(i))) {
						digit = false;
					}
				}
				if (!digit) {
					game.deal.setEnabled(false);
				} else if (game.betfield.getText().length() == 0
						|| Integer.parseInt(game.betfield.getText()) > game.player.balance
						|| Integer.parseInt(game.betfield.getText()) == 0) {
					game.deal.setEnabled(false);
				} else {
					game.deal.setEnabled(true);
				}
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					if ((game.deal.isEnabled())) {
						game.deal.doClick();
					}
				}
			}
		});

		/**
		 * Deal nuppu vajutades muudetakse m�ngu olekut. Esimeses olekus saab
		 * kasutaja m��rata panuse ning nuppu vajutades jagatakse talle 5 kaarti
		 * ja m�ng l�heb teise olekusse. Teises olekus saab kasutaja valida,
		 * millised kaardid ta v�lja vahetab ning Deal nuppu uuesti vajutades
		 * l�heb m�ng esimesse olekusse ning kasutajale n�idatakse, mis tal k�es
		 * on ning kui palju on tal raha alles.
		 * 
		 */
		game.deal.setEnabled(false);
		game.deal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int status = game.status;
				if (status == 1) {
					game.shoulddraw = false;
					game.animate();
					game.statuspanel.clearHand();
					repaint();
				} else if (status == 2) {
					game.animate();
					repaint();
				}
				repaint();
			}
		});
		game.add(game.betfield);
		game.add(game.deal);
		game.add(game.statuspanel);

		/**
		 * Kui klikkida mingile kaardile, muudab see hand objektis tolle kaardi
		 * olekut.
		 */
		game.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				game.buttonx = e.getX();
				game.buttony = e.getY();
				if (game.allowchange) {
					for (int i = 0; i < 5; i++) {
						if (game.buttony <= game.cardy && game.buttonx >= game.marginx * i
								&& game.buttonx <= game.marginx * i + game.cardx) {
							game.hand.changeCardStatus(i);
							repaint();
						}
					}
				}
			}

		});

	}
}
