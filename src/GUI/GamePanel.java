/**
 * Kujuteldav kast, kus toimub kogu graafiline tegevus.
 * 
 * @author Martin Paakspuu
 */
package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

import backend.Card;
import backend.Deck;
import backend.Hand;
import backend.Player;

public class GamePanel extends JPanel implements ActionListener {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	/** Kaartide pilt */
	private Image cardImages;
	/** Negatiivsetes v�rvides kaartide pilt */
	private Image cardImages2;
	/** Kaardi taguse pilt */
	private Image cardBack;
	/** Deck objekt */
	public Deck deck = new Deck();
	/** Hand objekt */
	public Hand hand = new Hand(deck.getHand(5));
	/** Player objekt */
	public Player player = new Player();
	/** Timer objekt */
	private Timer tm = new Timer(10, this);
	/** Kaardi liikumise kiirus */
	private double velX, velY;
	/** Massiiv, mis �tleb, kas kaardi tagust n�idatakse v�i mitte */
	private boolean[] shouldShowCover = new boolean[5];
	/** Kaartide vahetamise loa t�ev��rtus */
	public boolean allowchange = false;
	/** Kaartide n�itamise t�ev��rtus */
	public boolean shoulddraw = false;
	/** Kontrollitava kaardi koht massiivis */
	private int k = 0;
	/** M�ngu staatus */
	public int status = 1;
	/** Kaartide liikumise kiirus */
	private int speed = 20;
	/** Kaardi laius */
	public int cardx = 71;
	/** Kaardi pikkus */
	public int cardy = 96;
	/** Kaartidevaheline kaugus */
	public int marginx = 107;
	/** Liikuva kaardi asukoha x koordinaat */
	private int animationx = 0;
	/** Liikuva kaardi asukoha y koordinaat */
	public int animationy;
	/** Liikuva kaardi olek */
	private int time;
	/** Klikitava koha x koordinaat */
	public int buttonx;
	/** Klikitava koha y koordinaat */
	public int buttony;

	/** StatusPanel objekt */
	StatusPanel statuspanel = new StatusPanel();
	/** Panuse sisestamise aken */
	JTextField betfield = new JTextField();
	/** Deal nupp */
	JButton deal = new JButton("Deal");

	/**
	 * GamePanel t��pi objekti konstruktor
	 */
	public GamePanel() {
		this.setEnabled(true);
		this.setBackground(new Color(51, 143, 51));
		this.setLayout(null);
		loadImages();
	}

	/**
	 * Salvestab kaardi pildid isendimuutujatesse
	 */
	private void loadImages() {
		ClassLoader cl = getClass().getClassLoader();
		URL imageURL1 = cl.getResource("cards.png");
		URL imageURL2 = cl.getResource("negativecards.png");
		URL imageURL3 = cl.getResource("cardcover2.png");
		if (imageURL1 != null) {
			cardImages = Toolkit.getDefaultToolkit().createImage(imageURL1);
		} else {
			System.out.println("cards.png puudub");
		}
		if (imageURL2 != null) {
			cardImages2 = Toolkit.getDefaultToolkit().createImage(imageURL2);
		} else {
			System.out.println("negativecards.png puudub");
		}
		if (imageURL3 != null) {
			cardBack = Toolkit.getDefaultToolkit().createImage(imageURL3);
		} else {
			System.out.println("cardcover2.png puudub");
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < 5; i++) {
			g.drawRect(i * marginx, 0, cardx, cardy);
		}
		if (shoulddraw) {
			for (int i = 0; i < 5; i++) {
				if (hand.cardStatus[i] == true) {
					drawCard(g, hand.hand[i], i * marginx, 0, cardx, cardy);
				}
				if (hand.cardStatus[i] == false) {
					drawCardNeg(g, hand.hand[i], i * marginx, 0, cardx, cardy);
				}
			}
		}
		for (int i = 0; i < 5; i++) {
			if (shouldShowCover[i]) {
				drawBack(g, i * marginx, 0);
			}
		}
		drawBack(g, (int) ((double) (20 / 500) * this.getWidth()), this.getHeight() - cardy);
		drawBack(g, (int) ((double) (10 / 500) * this.getWidth()), this.getHeight() - cardy);
		drawBack(g, 0, this.getHeight() - cardy);
		drawBack(g, animationx, animationy);

	}

	/**
	 * Alustab kaartide liigutamist
	 */
	public void animate() {
		betfield.setEnabled(false);
		deal.setEnabled(false);
		allowchange = false;
		if (hand.cardStatus[k]) {
			getVelocity(k);
			tm.start();
		} else if (k == 4) {
			endAnimation();
		} else {
			k++;
			animate();
		}
	}

	/**
	 * Peatab kaardi liigutamise
	 */
	public void stopAnimation() {
		tm.stop();
		animationx = 0;
		animationy = this.getHeight() - cardy;
		shouldShowCover[k] = true;
		k++;
		time = 0;
		repaint();
		if (k == 5) {
			endAnimation();
		} else {
			animate();
		}
	}

	/**
	 * L�petab kaartide liigutamise
	 */
	private void endAnimation() {
		k = 0;
		for (int i = 0; i < 5; i++) {
			if (hand.cardStatus[i] == true) {
				hand.hand[i] = deck.Deal();
			}
		}
		for (int i = 0; i < 5; i++) {
			shouldShowCover[i] = false;
		}
		System.out.println(Arrays.toString(hand.getHandCards()));
		shoulddraw = true;
		repaint();
		if (status == 1) {
			allowchange = true;
			betfield.setEnabled(false);
			deal.setEnabled(true);
		}
		if (status == 2) {
			hand.getCardStatus();
			deck.Clear();
			int bet = Integer.parseInt(betfield.getText());
			betfield.setText("");
			betfield.setEnabled(true);
			player.getHand(hand.hand);
			player.Result(bet);
			statuspanel.GetNewBalance(player.balance);
			statuspanel.getHand(player.hand);
			if (player.balance == 0) {
				JOptionPane.showMessageDialog(null, "You lose!");
			}
		}

		changeStatus();
	}

	/**
	 * Arvutab kaartide liikumise kiirusvektorid
	 */
	public void getVelocity(int card) {
		velX = ((card * marginx - animationx) / speed);
		velY = (-animationy / speed);
	}

	/**
	 * Liigutab kaarte (timer)
	 */
	public void actionPerformed(ActionEvent e) {
		animationx += velX;
		animationy += velY;
		time++;
		repaint();
		if (time == speed) {
			stopAnimation();
		}
	}

	/**
	 * Joonistab kaardi
	 * 
	 * @param g
	 * @param card
	 *            Card t��pi objekt
	 * @param posx
	 *            kaardi x koordinaat
	 * @param posy
	 *            kaardi y koordinaat
	 * @param sizex
	 *            kaardi pikkus
	 * @param sizey
	 *            kaardi laius
	 */
	public void drawCard(Graphics g, Card card, int posx, int posy, int sizex, int sizey) {
		int cx = card.coordinates[0];
		int cy = card.coordinates[1];
		g.drawImage(cardImages, posx, posy, posx + sizex, posy + sizey, cx, cy, cx + 71, cy + 96, this);
	}

	/**
	 * Joonistab kaardi negatiivsetes v�rvides
	 * 
	 * @param g
	 * @param card
	 *            Card t��pi objekt
	 * @param posx
	 *            kaardi x koordinaat
	 * @param posy
	 *            kaardi y koordinaat
	 * @param sizex
	 *            kaardi pikkus
	 * @param sizey
	 *            kaardi laius
	 */
	public void drawCardNeg(Graphics g, Card card, int posx, int posy, int sizex, int sizey) {
		int cx = card.coordinates[0];
		int cy = card.coordinates[1];
		g.drawImage(cardImages2, posx, posy, posx + sizex, posy + sizey, cx, cy, cx + 71, cy + 96, this);
	}

	/**
	 * Joonistab kaardi taguse
	 * 
	 * @param g
	 * @param x
	 *            kaardi taguse laius
	 * @param y
	 *            kaardi taguse pikkus
	 */
	public void drawBack(Graphics g, int x, int y) {
		g.drawImage(cardBack, x, y, x + cardx + 2, y + cardy + 3, 0, 0, 100, 141, this);
	}

	/**
	 * Muudab m�ngu olekut
	 */
	public void changeStatus() {
		if (status == 1) {
			status = 2;
		} else if (status == 2) {
			status = 1;
		}
	}
}